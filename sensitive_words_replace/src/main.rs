use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use regex::Regex;
use rand::{thread_rng};
use rand::prelude::IndexedRandom;

#[derive(Deserialize, Serialize)]
struct Request {
    output1: String,
}

#[derive(Deserialize, Serialize)]
struct Response {
    output: String,
    level: i32,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let non_sensitive_words = vec!["happy", "friendly", "safe", "inclusive", "tolerant", "respectful", "peaceful", "kind", "moderate", "accepting", "progressive", "calm"];

    let re = Regex::new(r"\*\*\*\*").map_err(|e| {
        eprintln!("Failed to compile regex: {}", e);
        Error::from(e)
    })?;

    let mut rng = thread_rng();

    let result = re.replace_all(&event.payload.output1, |_caps: &regex::Captures| {
        let non_sensitive_word = non_sensitive_words.choose(&mut rng).unwrap_or(&"");
        non_sensitive_word.to_string()
    }).to_string();

    let count = event.payload.output1.matches("****").count();
    let level = count as i32;

    Ok(Response { output: result, level })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
