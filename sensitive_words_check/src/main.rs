use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use regex::Regex;

#[derive(Deserialize, Serialize)]
struct Request {
    input: String,
}

#[derive(Deserialize, Serialize)]
struct Response {
    output1: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let sensitive_words = vec!["sexist", "evil", "dangerous", "racist", "supremacist", "blasphemy", "terrorism", "extremist", "harassment", "pornography", "heresy", "violence"];

    let pattern = sensitive_words.join("|");
    let re = Regex::new(&pattern).map_err(|e| {
        eprintln!("Failed to compile regex: {}", e);
        Error::from(e)
    })?;

    let result = re.replace_all(&event.payload.input, "****").to_string();

    Ok(Response { output1: result })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}
