## Rust AWS Lambda and Step Functions

The project aims to leverage Rust for building an AWS Lambda function to process data efficiently. Additionally, it utilizes AWS Step Functions to orchestrate a workflow that coordinates multiple Rust Lambda functions, forming a data processing pipeline. The goal is to showcase the power of Rust in serverless computing and demonstrate its ability to handle complex data workflows seamlessly on AWS infrastructure.

### Rust Lambda Function for Sensitive Word Filtering

This Rust Lambda function is designed to filter sensitive words from input text. It is built using the `lambda_runtime` crate, enabling it to be deployed as an AWS Lambda function.

Components:

1. Request Struct:
- Deserialized from JSON, it contains a field `input` representing the input text to be processed.
2. Response Struct:
- Serialized into JSON, it contains a field `output1` representing the processed output text.
3. Function Handler:
- The function_handler function processes incoming Lambda events, filtering out sensitive words from the input text using regular expressions.
- It replaces sensitive words with `****` and returns the processed output.
4. Main Function:
- The `main` function initializes the Lambda service, setting up the function handler and running the Lambda function.

Workflow:
1. When invoked, the Lambda function receives a request containing input text.
2. It processes the input text, filtering out sensitive words using regular expressions.
3. The filtered output text is returned as a response.

Use Case:
This Lambda function can be integrated into applications to automatically filter out sensitive content from user-generated text, helping to maintain a safe and respectful online environment.

### Rust Lambda Function for Sensitive Word Filtering with Level Count

This Rust Lambda function is designed to filter sensitive words from input text and count the occurrences of **** in the output text. It is built using the lambda_runtime crate and can be deployed as an AWS Lambda function.

Components:
1. Request Struct:

- Deserialized from JSON, it contains a field `output1` representing the input text to be processed.
2. Response Struct:

- Serialized into JSON, it contains two fields:
- output: Represents the processed output text with sensitive words replaced by non-sensitive words.
- level: Represents the count of `****` occurrences in the output text.
3. Function Handler:

- The `function_handler` function processes incoming Lambda events, filtering out sensitive words from the input text using regular expressions.
- It replaces sensitive words with randomly chosen non-sensitive words and counts the occurrences of `****` in the output text.
4. Main Function:

- The main function initializes the Lambda service, setting up the function handler and running the Lambda function.
Workflow:
1. When invoked, the Lambda function receives a request containing the input text.
2. It processes the input text, replacing sensitive words with randomly chosen non-sensitive words using regular expressions.
3. The count of `****` occurrences in the output text is calculated.
4. The processed output text and the count of `****` occurrences are returned as a response.

Use Case:
This Lambda function can be integrated into applications to automatically filter out sensitive content from user-generated text and provide insights into the severity of the filtered content by counting the occurrences of `****`.

### Project Setups
#### Project Start
1. Clone the GitLab repository and cd into sensitive_words_check or sensitive_words_replace.
2. Attach Rust project to `cargo.toml`.
3. Run the following code to build the project.
```shell
cargo lambda build
```
#### Test Locally
1. Navigate into sensitive_words_check or sensitive_words_replace.
2. Run the following code to test locally.
```shell
cargo lambda watch
```
3. Test with provided JSON template.
```shell
cargo lambda invoke --data-file ./<JSON TEMPLATE> 
```
#### Deploy on AWS Lambda
1. Create a IAM Role with full access to lambda function: `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, and `IAMFullAccess`. 
2. Build the project.
```shell
cargo lambda build --release
```
3. Set up the AWS account info with AWS CLI. 
```shell
aws configure
```
4. Deploy the project to AWS Lambda.
```shell
cargo lambda deploy --iam-role <ROLE ARN>
```

### Implement Step Function of State Machine
1. Create a new state machine.
2. Write code to build the workflow, which defines an AWS Step Functions state machine workflow aimed at checking for sensitive words and replacing them with non-sensitive alternatives. It begins with the "CheckSensitiveWords" task state, which invokes a Lambda function to perform the sensitivity check. Upon completion, it transitions to the "ReplaceSensitiveWords" task state, where another Lambda function is invoked to replace sensitive words. Finally, the workflow ends after the replacement task, as indicated by the "End" field set to true.

![Screenshot 2024-04-16 at 10.25.34 AM.png](screenshots%2FScreenshot%202024-04-16%20at%2010.25.34%20AM.png)

### Demo Video

You can find the demo video of the project [here](https://gitlab.oit.duke.edu/xc202/ids721-individual-project4-xc202/-/blob/main/Demo_Video.mov).

### Results

Test Locally of Sensitive Word Filtering: 

![Screenshot 2024-04-16 at 10.00.38 AM.png](screenshots%2FScreenshot%202024-04-16%20at%2010.00.38%20AM.png)

Test Locally of Sensitive Word Filtering with Level Count

![Screenshot 2024-04-16 at 9.41.39 AM.png](screenshots%2FScreenshot%202024-04-16%20at%209.41.39%20AM.png)

Execution Results of State Machine:

![Screenshot 2024-04-16 at 10.26.39 AM.png](screenshots%2FScreenshot%202024-04-16%20at%2010.26.39%20AM.png)

![Screenshot 2024-04-16 at 10.26.59 AM.png](screenshots%2FScreenshot%202024-04-16%20at%2010.26.59%20AM.png)